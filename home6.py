def min_arg(*ls):
    return min(ls)

def min_0_255(*ls):
    lc = list(ls)
    for i in lc:
        if i<0 or i>255:
            lc.remove(i)
    return min(lc)


def param(*tup,**dict):
    print(list(tup))
    print(dict)
    pass


def fibo(i,f2=1,f1 = 1):
    z = f1 + f2
    f1 = f2
    f2 = z
    i -= 1
    if i > 0:
        f2 = fibo(i,f2,f1)
    return f2


param(7,1,5,8,q=3,c=2)
print(fibo(8))

ls = [10,1,5,-28,1,20]
print(min_arg(ls[0],ls[1],ls[4],ls[5]))
print(min_0_255(ls[0],ls[1],ls[3],ls[5]))